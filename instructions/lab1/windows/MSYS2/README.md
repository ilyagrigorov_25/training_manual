# Установка MSYS2

Следуйте инструкциям по установке на [веб-сайте MSYS2](https://www.msys2.org/), чтобы установить Mingw-w64. Обязательно запустите все необходимые меню и команду `pacman`.

Шаг 1:

![Установка MSYS2 - шаг 1](images/MSYS2-install-step1.png)

Шаг 2:

![Установка MSYS2 - шаг 2](images/MSYS2-install-step2.png)

Шаг 3:

![Установка MSYS2 - шаг 3](images/MSYS2-install-step3.png)

Шаг 4:

![Установка MSYS2 - шаг 4](images/MSYS2-install-step4.png)

Шаг 5:

![Установка MSYS2 - шаг 5](images/MSYS2-install-step5.png)

Шаг 6 (ожидание установки):

![Установка MSYS2 - шаг 6](images/MSYS2-install-step6.png)

Шаг 7 (завершение установки):

![Установка MSYS2 - шаг 7](images/MSYS2-install-step7.png)

В открытый терминал необходимо ввести команду

```bash
pacman -S mingw-w64-x86_64-gcc
```

и подтвердить действие:

![Установка MSYS2 - шаг 8](images/MSYS2-install-step8.png)

Также выполните команду:

```bash
pacman -S --needed base-devel mingw-w64-x86_64-toolchain
```

![Установка MSYS2 - шаг 9](images/MSYS2-install-step9.png)

После завершения работы pacman добавьте путь `C:\msys64\mingw64\bin` в переменную среды Windows `PATH`.

Чтобы открыть переменные среды Windows вы можете использовать поиск в панели задач (начните вводить "Переменных") или нажать клавиши `Win+R` на клавиатуре, ввести `sysdm.cpl` и нажать `Enter`:

![Установка MSYS2 - шаг 10](images/MSYS2-install-step10.png)

Подтвердите изменение в открытом и родительских окнах.

В терминале проверьте результат установки:

```bash
g++ --version
gdb --version
```

![Установка MSYS2 - шаг 11](images/MSYS2-install-step11.png)
