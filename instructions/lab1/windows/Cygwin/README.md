# Установка Cygwin

## Подготовка к установке пакетов

`Cygwin` - UNIX-подобная среда и интерфейс командной строки для Microsoft Windows. Установщик можно скачать с [сайта](https://cygwin.com/install.html).

![Скачивание Cygwin](images/cygwin-download.png)

После загрузки необходимо запустить установщик и проследовать по экранным формам до выбора пакетов для установки.

Шаг 1:

![Установка Cygwin - шаг 1](images/cygwin-install-step1.png)

Шаг 2:

![Установка Cygwin - шаг 2](images/cygwin-install-step2.png)

Шаг 3:

![Установка Cygwin - шаг 3](images/cygwin-install-step3.png)

Шаг 4:

![Установка Cygwin - шаг 4](images/cygwin-install-step4.png)

Шаг 5:

![Установка Cygwin - шаг 5](images/cygwin-install-step5.png)

Шаг 6:

![Установка Cygwin - шаг 6](images/cygwin-install-step6.png)

Шаг 7 (выбор пакетов):

![Установка Cygwin - шаг 7](images/cygwin-install-step7.png)

## Установка пакетов

На экранной форме в поле `search` вводятся пакеты, которые можно установить. Требуется установить следующие пакеты:

- `gcc-g++` (последняя актуальная версия)
- `gcc-core` (последняя актуальная версия)
- `gdb` (последняя актуальная версия)

Пакет `gcc-g++`:

![Пакеты Cygwin - gcc-g++](images/cygwin-packages-step1.png)

Пакет `gcc-core`:

![Пакеты Cygwin - gcc-core](images/cygwin-packages-step2.png)

Пакет `gdb`:

![Пакеты Cygwin - gcc-gdb](images/cygwin-packages-step3.png)

Выбранные пакеты устанавливаем.

Шаг 1:

![Установка пакетов Cygwin - шаг 1](images/cygwin-install-packages-step1.png)

Шаг 2:

![Установка пакетов Cygwin - шаг 2](images/cygwin-install-packages-step2.png)

Шаг 3 (ожидание установки):

![Установка пакетов Cygwin - шаг 3](images/cygwin-install-packages-step3.png)

Шаг 4 (завершение установки):

![Установка пакетов Cygwin - шаг 4](images/cygwin-install-packages-step4.png)

## Добавление папки Cygwin в переменные окружения

Чтобы открыть переменные среды Windows вы можете использовать поиск в панели задач (начните вводить "Переменных") или нажать клавиши `Win+R` на клавиатуре, ввести `sysdm.cpl` и нажать `Enter`:

![Переменные окружения Cygwin - шаг 1](images/cygwin-env-step1.png)

Откройте пункт "Изменение системных переменных среды":

![Переменные окружения Cygwin - шаг 2](images/cygwin-env-step2.png)

Откройте на редактирование переменную среды пользователя `Path`:

![Переменные окружения Cygwin - шаг 3](images/cygwin-env-step3.png)

Добавьте в переменную полный путь папки `bin` Cygwin (по умолчанию это папка `C:\cygwin64\bin`):

![Переменные окружения Cygwin - шаг 4](images/cygwin-env-step4.png)

Подтвердите изменение в открытом и родительских окнах.

## Проверка правильности установки Cygwin

Запустите терминал Windows. Чтобы открыть терминал вы можете использовать поиск в панели задач (начните вводить "Терминал") или нажать клавиши `Win+R` на клавиатуре, ввести `cmd` и нажать `Enter`.

В терминале необходимо выполнить следующие команды

```powershell
gcc --version
gdb --version
```

В случае успешной установки вывод будет таким:

![Переменные окружения Cygwin - шаг 5](images/cygwin-env-step5.png)
