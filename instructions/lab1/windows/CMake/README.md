# Установка CMake

CMake — это кроссплатформенная утилита, обладающая возможностями автоматизации сборки программного обеспечения из исходного кода. Скачать можно [здесь](https://cmake.org/download/):

![Установка CMake - шаг 1](images/cmake-install-step1.png)

![Установка CMake - шаг 2](images/cmake-install-step2.png)

Указываем, что CMake необходимо добавить в переменную окружения `PATH`:

![Установка CMake - шаг 3](images/cmake-install-step3.png)

Указываем куда установить:

![Установка CMake - шаг 4](images/cmake-install-step4.png)

![Установка CMake - шаг 5](images/cmake-install-step5.png)

![Установка CMake - шаг 6](images/cmake-install-step6.png)

Далее требуется скачать [по ссылке](https://github.com/ninja-build/ninja/releases) последнюю версию Ninja и распаковать архив в `C:\Program Files\CMake\bin` (куда был установлен CMake):

![Установка CMake - шаг 7](images/cmake-install-step7.png)

![Установка CMake - шаг 8](images/cmake-install-step8.png)

В терминале проверить работу cmake и ninja:

```powershell
cmake --version
ninja --version
```

![Установка CMake - шаг 9](images/cmake-install-step9.png)
